# API bridge

Service intergiciel permettant la transformation des requêtes JSON/REST en provenance de l'API Gateway de l'application Zéphir en messages WAMP.

## Principe

Le service api-bridge permet d'émettre des messages sur le message broker WAMP de l'application Zéphir.

Il applique une transformation simple du corps JSON de la requête HTTP pour en faire un message WAMP.

### Translation URL vers topic du message

L'URL de la requête HTTP est utilisée pour générer le topic du message WAMP via le schéma suivant:

### Structuration des données

Les données sont attendues au format JSON côté serveur HTTP et doivent suivre le modèle suivant:

```json
{
 "clé1": "value1"
}
```

Tous les types autorisés par le format JSON sont valides pour les valeurs du tableau `args` et les valeurs du dictionnaire `kwargs` (aucune validation n'est effectuée sur les types par le service api-bridge).
