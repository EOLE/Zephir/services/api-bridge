from zephir.message import list_messages, parse_definition, get_message
from collections import OrderedDict
from os.path import join, basename, dirname
from glob import glob

from tiramisu import StrOption, IntOption, BoolOption, ChoiceOption, OptionDescription, Config, SymLinkOption
ALLOW_PRIVATE = False
ROOTPATH = join('..')


def _get_description(description,
                     name):
    # hack because some description are, in fact some help
    if not '\n' in description and len(description) <= 150:
        doc = description
    else:
        doc = name
    if doc.endswith('.'):
        doc= description[:-1]
    return doc


def _get_option(name,
                arg,
                file_path,
                select_option,
                optiondescription):
    """generate option
    """
    props = []
    if not hasattr(arg, 'default'):
        props.append('mandatory')
    description = arg.description.strip().rstrip()
    kwargs = {'name': name,
              'doc': _get_description(description, name),
              'properties': frozenset(props),
              'multi': arg.multi,
              'requires': [{'option': select_option,
                            'expected': optiondescription,
                            'action': 'disabled',
                            'inverse': True}]}
    if hasattr(arg, 'default'):
        kwargs['default'] = arg.default
    type_ = arg.type
    if type_ == 'Dict' or 'String' in type_ or 'Any' in type_:
        return StrOption(**kwargs)
    elif 'Number' in type_ or type_ == 'ID' or type_ == 'Integer':
        return IntOption(**kwargs)
    elif type_ == 'Boolean':
        return BoolOption(**kwargs)
    raise Exception('unsupported type {} in {}'.format(type_, file_path))


def _parse_args(message_def,
                options,
                file_path,
                needs,
                select_option,
                optiondescription):
    """build option with args/kwargs
    """
    new_options = OrderedDict()
    for name, arg in message_def.parameters.items():
        new_options[name] = arg
        if arg.ref:
            needs.setdefault(message_def.uri, {}).setdefault(arg.ref, []).append(name)
    for name, arg in new_options.items():
        current_opt = _get_option(name, arg, file_path, select_option, optiondescription)
        options.append(current_opt)
        if arg.shortarg:
            options.append(SymLinkOption(arg.shortarg, current_opt))


def _parse_responses(message_def,
                     file_path):
    """build option with returns
    """
    responses = OrderedDict()
    if message_def.response:
        keys = {'': {'description': '',
                     'columns': {}}}
        provides = {}
        to_list = True
        param_type = message_def.response.type

        if param_type.startswith('[]'):
            to_list = False
            param_type = param_type[2:]
        if param_type in ['Dict', 'File']:
            pass

        if message_def.response.parameters is not None:
            for name, obj in message_def.response.parameters.items():
                if name in responses:
                    raise Exception('multi response with name {} in {}'.format(name, file_path))

                descr = obj.description.strip().rstrip()
                keys['']['columns'][name] = {'description': obj.description,
                                                    'type': obj.type}
                ref = obj.ref
                if ref:
                    provides[name] = ref
            else:
                keys['']['columns'][name] = {'description': descr,
                                             'type': obj.type}
                ref = obj.ref
                if ref:
                    provides[name] = ref
                responses['keys'] = keys
                responses['to_list'] = to_list
                responses['to_dict'] = False
                responses['provides'] = provides
        else:
            name = 'response'
            keys['']['columns'][name] = {'description': message_def.response.description,
                                         'type': message_def.response.type}
            ref = message_def.response.ref
            if ref:
                provides[name] = ref
            responses['keys'] = keys
            responses['to_list'] = to_list
            responses['to_dict'] = True
            responses['provides'] = provides
    return responses


def _getoptions_from_yml(message_def,
                         version,
                         optiondescriptions,
                         file_path,
                         needs,
                         select_option):
    if message_def.pattern == 'event' and message_def.response:
        raise Exception('event with response?: {}'.format(file_path))
    if message_def.pattern == 'rpc' and not message_def.response:
        print('rpc without response?: {}'.format(file_path))
    options = [StrOption('version',
                         'version',
                         version,
                         properties=frozenset(['hidden']))]
    _parse_args(message_def, options, file_path, needs, select_option, message_def.uri)
    name = message_def.uri
    description = message_def.description.strip().rstrip()
    optiondescriptions[name] = (description, options)


def _get_root_option(select_option, optiondescriptions):
    """get root option
    """
    def _get_od(curr_ods):
        options = []
        for name in curr_ods.keys():
            if name is None:
                description, curr_options = curr_ods[name]
                options.extend(curr_ods[name][1])
            else:
                if None in curr_ods[name].keys():
                    description = curr_ods[name][None][0]
                    if description.endswith('.'):
                        description = description[:-1]
                else:
                    description = None
                options.append(OptionDescription(name,
                                                 description,
                                                 _get_od(curr_ods[name])))
        return options

    options_obj = [select_option]
    struct_od = {}
    for od_name, options_descr in optiondescriptions.items():
        # od_name is something like config.configuration.server.get
        curr_od = struct_od
        for subod in od_name.split('.'):
            curr_od.setdefault(subod, {})
            curr_od = curr_od[subod]
        # curr_od is now {'config': {'configuration': {server: {}}}}
        curr_od[None] = options_descr
        # curr_od is now {'config': {'configuration': {server: {None: options_descr}}}}
    options_obj.extend(_get_od(struct_od))
    return OptionDescription('root', 'root', options_obj)


def get_messages():
    """generate description from yml files
    """
    optiondescriptions = OrderedDict()
    optiondescriptions_name = []
    responses = OrderedDict()
    needs = OrderedDict()
    messages = list(list_messages())
    messages.sort()
    for message_name in messages:
        message_def = get_message(message_name)
        if message_def.pattern not in ['rpc', 'event'] or \
                (not message_def.public and not ALLOW_PRIVATE):
            continue
        optiondescriptions_name.append(message_def.uri)
    optiondescriptions_name.sort()
    select_option = ChoiceOption('message',
                                 'Nom du message.',
                                 tuple(optiondescriptions_name),
                                 properties=frozenset(['mandatory', 'positional']))
    for message_name in messages:
        message_def = get_message(message_name)
        if message_def.pattern not in ['rpc', 'event'] or \
                (not message_def.public and not ALLOW_PRIVATE):
            continue
        version = message_name.split('.')[0]
        if message_def.uri in responses:
            raise Exception('uri {} allready loader'.format(message_def.uri))
        _getoptions_from_yml(message_def,
                             version,
                             optiondescriptions,
                             message_name,
                             needs,
                             select_option)
        responses[message_def.uri] = _parse_responses(message_def,
                                                      message_name)

    root = _get_root_option(select_option, optiondescriptions)
    try:
        config = Config(root)
    except Exception as err:
        raise Exception('error when generating root optiondescription: {}'.format(err))

    config.property.read_write()
    config.property.add('demoting_error_warning')
    return needs, responses, config
